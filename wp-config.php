<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_sejagiagante_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R#Syw@D_oG>dV,Q#m==c~u{oPz@lk%#({K<CR6faE$JX9QW%l@;GL:at$2px}|LH');
define('SECURE_AUTH_KEY',  '.$rbUG_Xy*8j-4md]}FFtYUQBv)OJ5=YM9taX.q *k*A8gYTP@b&0_:?[n8;R%nW');
define('LOGGED_IN_KEY',    '#zrD2+//ti>$]9/q?9^2Q3oM<?UZ}XD~1NACAEJvOXn)|Reb^(H00`R+#Cv{TTa+');
define('NONCE_KEY',        '5u4x-*!taw[Zc=~[1Q#*knR})`B1l$PWq[A;F1zC+F(k6TB./Y`&UG|ME*2ilb-p');
define('AUTH_SALT',        '8e*#Q;t Ex#Hb_s960sO7N`ysArk2n9RJ3>|9if9T<4^#RF$Okw6*R~yi}fC-<qx');
define('SECURE_AUTH_SALT', 'yh~rz]=,8lYDH ^qO1o^4gegxDwpmt/B.j*-&#f$n?c,i:ej{ywz,]PsD@j4~wy,');
define('LOGGED_IN_SALT',   '-Sg`rX@Wm@xRl#S )W/L|,BBgwgRiwf8d&4-Y<(v2(=(V}C7U0M[do{[QSs--o`d');
define('NONCE_SALT',       '12p]l?G;iH}2)buBzorw%(UmO9)z-T#i;AJPGoZ&byGFfxmWV*RB_Z^W#TP}~4X(');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'jm_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
define ('WP_MEMORY_LIMIT', '256M');
/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');


