<?php
/**
 * Template Name: Inicial
 * @package Juliana_Mimura
 */
global $configuracao;
get_header();
?>

<div class="pg-inicial" style="">
			<section class="carrossel" id="carrosselDestaque">

				<?php 

				$item_carrossel_destaque = new WP_Query( array ( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

				while( $item_carrossel_destaque -> have_posts() ): $item_carrossel_destaque->the_post();

					$foto_destaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

					$foto_destaque = $foto_destaque[0];

				?>
				
				<div class="item">
					<figure style="background:url(<?php echo $foto_destaque ?>)"></figure>
				</div>

				<?php endwhile; wp_reset_query() ?>
				
			</section>

			<!-- <div class="text-center">
				<a href="http://sejagigante.com.br/downloads/" style="background: #921521;
				    color: #fff;
				    padding: 20px;
				    display: inline-block;
				    margin: 0 auto;
				    border-radius: 5px;
				    text-transform: uppercase;
				    font-weight: 600;
				    font-size: 13px;">Quero Minha Ferramenta Agora!</a>
		    </div> -->

			<section class="sobre" id="sobre">

				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="texto">
								<h3>Sobre</h3>
								<p><?php echo $configuracao['opt_desc_sobre'] ?></p>
							</div>
						</div>
						<div class="col-sm-6">
							<figure class="sobreFoto" style="background: url(<?php echo $configuracao['opt_img_sobre']['url'] ?>)">
							</figure>
						</div>
					</div>

					<div class="texto2">
						<h3>Minha História</h3>
						<p><?php echo $configuracao['opt_texto_mh_p1'] ?></p>
						<p><?php echo $configuracao['opt_texto_mh_p2'] ?></p>

						<div class="topico">
						<h3>Como surgiu a Seja Gigante?</h3>
						
						<p><?php echo $configuracao['opt_texto_cs_p1'] ?></p>
						</div>
						<div class="foto">
							
							<figure>
							<img src="<?php echo $configuracao['opt_imgs1_como_surgiu']['url'] ?>" alt="">
							</figure>

							<figure>
								<img src="<?php echo $configuracao['opt_imgs2_como_surgiu']['url'] ?>" alt="">
							</figure>

							<figure>
								<img src="<?php echo $configuracao['opt_imgs3_como_surgiu']['url'] ?>" alt="">
							</figure>
						</div>

						<p><?php echo $configuracao['opt_texto_cs_p2'] ?></p>
						<p><?php echo $configuracao['opt_texto_cs_p3'] ?></p>
						<p><?php echo $configuracao['opt_texto_cs_p4'] ?></p>

					</div>
				</div>
			</section>

			<section class="newssllater" style="background-image: url('<?php echo $configuracao['opt_background_form']['url'] ?>');">
				<div class="form">
					<div class="textoNews">
						<p>Junte-se a outros gigantes!</p>
						<span>Receba conteúdos  para que você e sua empresa cresçam juntos.</span>
					</div>
						<!--START Scripts : this is the script part you can add to the header of your theme-->
						<script type="text/javascript" src="http://sejagigante.com.br/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
						<script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
						<script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
						<script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
						<script type="text/javascript">
							/* <![CDATA[ */
							var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://sejagigante.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
							/* ]]> */
						</script><script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
						<!--END Scripts-->


						<div class="widget_wysija_cont html_wysija">
							<div id="msg-form-wysija-html5bf2aa7bcd67f-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html5bf2aa7bcd67f-2" method="post" action="#wysija" class="widget_wysija html_wysija">
							

									<input type="text" name="wysija[user][firstname]" class="validate[required]" title="Nome" placeholder="Nome" value="" />



									<span class="abs-req">
										<input type="text" name="wysija[user][abs][firstname]" class="validated[abs][firstname]" value="" />
									</span>

							

									<input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Email" value="" />



									<span class="abs-req">
										<input type="text" name="wysija[user][abs][email]" class="validated[abs][email]" value="" />
									</span>

							

								<input class=" wysija-submit-field" type="submit" value="Cadastrar!" />

								<input type="hidden" name="form_id" value="2" />
								<input type="hidden" name="action" value="save" />
								<input type="hidden" name="controller" value="subscribers" />
								<input type="hidden" value="1" name="wysija-page" />


								<input type="hidden" name="wysija[user_list][list_ids]" value="1" />

							</form>
						</div>

					</div>
			</section>

			<section class="depoimentos" id="resultados">
				<h3>Resultados de alguns gigantes</h3>
				
				<div class="container">

						<?php 

						$contador = 0;
						$depoimentos = new WP_Query( array( 'post_type' => 'resultado', 'orderby' => 'id', 'order' => 'asc', 'post_per_page' => -1 ) );

						while( $depoimentos->have_posts() ): $depoimentos -> the_post();

							$img_depoimentos = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

							$img_depoimentos = $img_depoimentos[0];
							$contador++;

							if($contador % 2 != 0):

						?>

						<div class="row">

							<div class="col-sm-6 colMobile" style="display: none">
								<div class="fotoDepoimentoMobile">
									<img src="<?php echo $img_depoimentos ?>" alt="">
									<h3><?php echo rwmb_meta('Sejagigante_legenda_foto') ?></h3>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="depoimento">

									<h2><?php echo the_title(); ?></h2>
									
									<div class="textoDep">

										<p><?php echo the_content(); ?></p>

									</div>
								</div>
							</div>
							<div class="col-sm-6 colDesk">

								<figure class="depoimentoFoto">
									<img src="<?php echo $img_depoimentos ?>" alt="">
									<h3><?php echo rwmb_meta('Sejagigante_legenda_foto') ?></h3>
								</figure>

							</div>
						</div>

					<?php else: ?>

						<div class="row">
						
						<div class="col-sm-6">

							<figure class="depoimentoFoto">
								<img src="<?php echo $img_depoimentos ?>" alt="">
								<h3><?php echo rwmb_meta('Sejagigante_legenda_foto') ?></h3>
							</figure>

						</div>
						<div class="col-sm-6">
							<div class="depoimento">

								<h2><?php echo the_title(); ?></h2>

								<div class="textoDep">

									<p><?php echo the_content(); ?></p>
									
								</div>
							</div>
						</div>
					</div>

					<?php endif; endwhile; wp_reset_query(); ?>

				</div>
			</section>

			<section class="programa" id="programas">
				<div class="container">
					<h3 class="titulop">Programas</h3>
					<div class="row">
						<div class="col-sm-6">
							<div class="eusouGigante" id="euGigante">
								<div class="titutlo">

									<p><?php echo $configuracao['opt_desc_eu_gigante']; ?></p>

								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="icones">
								<p>O programa Eu Gigante proporciona que você tenha:</p>
								<ul>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img_eu_gigante']['url']; ?>" alt="">
										</figure>
										<h2> <span>+</span>  Organização</h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img1_eu_gigante']['url']; ?>" alt="">
										</figure>
										<h2> <span>+</span>  Produtividade </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img2_eu_gigante']['url']; ?>" alt="">
										</figure>
										<h2> <span>+</span>  Foco </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img3_eu_gigante']['url']; ?>" alt="">
										</figure>
										<h2> <span>+</span>  Liderança </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img4_eu_gigante']['url']; ?>" alt="">
										</figure>
										<h2> <span>-</span>  Stress e Ansiedade </h2>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>


			<section class="programa" id="empresaGigante">
				<div class="container">
					<div class="row">

						<div class="col-sm-6 colDesk">
							<div class="icones">
								<p>O programa Empresa Gigante proporciona:</p>
								<ul>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Lucratividade</h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img1_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Visão estratégica </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img2_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Assertividade na tomada de decisão </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img3_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Vantagem competitiva no Mercado </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img4_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Criatividade na solução de problemas  </h2>
									</li>
								</ul>
							</div>

						</div>

						<div class="col-sm-6">
							<div class="eusouGigante">
								<div class="titutlo">

									<p><?php echo $configuracao['opt_desc_empresa_gigante'] ?></p>

								</div>
							</div>
						</div>

						<div class="col-sm-6 colMobile" style="display: none;">
							<div class="icones">
								<p>O programa Empresa Gigante proporciona:</p>
								<ul>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Lucratividade</h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img1_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Visão estratégica </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img2_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Assertividade na tomada de decisão </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img3_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Vantagem competitiva no Mercado </h2>
									</li>
									<li>
										<figure>
											<img src="<?php echo $configuracao['opt_img4_empresa_gigante']['url']; ?>" alt="">
										</figure>
										<h2><span>+</span> Criatividade na solução de problemas  </h2>
									</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</section>

		 	<section class="contato" id="contato">
				<h3>Vamos crescer juntos? </h3>
				<p>Entre em contato</p>
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="8" title="Formulário de contato 1"]'); ?>
					<!-- <div class="row">
						<div class="col-sm-4">
							<div class="inputs">
								<input type="text" placeholder="Nome">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="inputs">
								<input type="text" placeholder="E-mail">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="inputs">
								<input type="tel" placeholder="Telefone">
							</div>
						</div>
					</div>
				</div>

				<div class="inputs">
					<textarea placeholder="Mensagem"></textarea>
					<input type="submit">
				</div> -->
			   
				</div>
			</section>

			<section class="instagram">
				<div class="tituloInstagram">
					<p>Seja Gigante todo dia!</p>
					<span>Siga no instagram <strong>@seja.gigante</strong> </span>
				</div>
				<div id="instafeed">

				</div>
			</section>
		</div>


		<script>
		// Bind no input e propertychange para pegar ctrl-v
		// e outras formas de input
		$(".telefone").bind('input propertychange',function(){
		    // pego o valor do telefone
		    var texto = $(this).val();
		    // Tiro tudo que não é numero
		    texto = texto.replace(/[^\d]/g, '');
		    // Se tiver alguma coisa
		    if (texto.length > 0)
		    {
		    // Ponho o primeiro parenteses do DDD    
		    texto = "(" + texto;

		        if (texto.length > 3)
		        {
		            // Fecha o parenteses do DDD
		            texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');  
		        }
		        if (texto.length > 12)
		        {      
		            // Se for 13 digitos ( DDD + 9 digitos) ponhe o traço no quinto digito            
		            if (texto.length > 13) 
		                texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
		            else
		             // Se for 12 digitos ( DDD + 8 digitos) ponhe o traço no quarto digito
		                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
		        }   
		            // Não adianta digitar mais digitos!
		            if (texto.length > 15)                
		               texto = texto.substr(0,15);
		    }
		    // Retorna o texto
		   $(this).val(texto);     
		})
		</script>
		

		<!-- <script type='text/javascript' src='http://code.jquery.com/jquery-compat-git.js'></script>
	    <script type='text/javascript' src='http://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script> -->
	    <!-- <script>
	        var behavior = function (val) {
	            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	        },
	        options = {
	            onKeyPress: function (val, e, field, options) {
	                field.mask(behavior.apply({}, arguments), options);
	            }
	        };

	        $('.telefone').mask(behavior, options);
	    </script> -->




<!-- <script>
	 var userFeed = new Instafeed({
       get: 'user',
       userId: '245759082',
       clientId: 'a1545960a7d345029a42367a9a2c0a4a',
       accessToken: '245759082.1677ed0.971992e7e1be49efadcb3f4d635dae03',
       resolution: 'standard_resolution',
       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
       sortBy: 'most-recent',
       limit: 6,
       links: false
     });
     userFeed.run();
</script> -->



<?php get_footer(); ?>