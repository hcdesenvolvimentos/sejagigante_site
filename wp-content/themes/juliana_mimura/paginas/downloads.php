<?php
/**
 * Template Name: Downloads
 * @package Juliana_Mimura
 */
global $configuracao;

get_header();

?>


<div class="pg pg-newsletter">
            <div class="containerFull">
                <div class="destaque">
                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-7">
                            <div class="texto">
                                <h1>Quer ter vantagem competitiva no mercado e não sabe por onde começar?</h1>
                                <h3 class="primeiro-p">Receba gratuitamente a ferramenta que propicia uma visão ampla do seu negócio e te possibilita pensar globalmente e agir localmente.</h3>
                                <h3 class="segundo-p">Juliana Mimura, Master coach e estrategista de negócios, disponibiliza o passo a passo para aplicar a ferramenta e ter ideias práticas para ter mais sucesso no seu negócio.</h3>
                                <div class="botao">
                                    <button class="minha-ferramenta">Quero Minha Ferramenta Agora!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="documentoPdf">
                    <h3>Inscreva-se agora e receba gratuitamente a Ferramenta 6 ”Rs”</h3>
                    <div class="imagemPdf">
                        <figure></figure>
                    </div>
                    <p>Ferramenta 6 ”Rs”</p>
                    <div class="desc">
                        <p>Com o aumento da competitividade, do ritmo das mudanças nos negócios e o surgimento repentino de desafios e oportunidades sem precedentes, há uma necessidade de ter um gerenciamento flexível e dinâmico. Não é simplesmente o que sabemos que importa, mas como reagimos ao que não sabemos. Na prática, isso significa ter sucesso em seis grandes áreas: liderar com um propósito e razão, gerar e maximizar receitas, despertar pessoas e sonhos, construir relacionamentos, maximizar uma boa reputação e tornar-se resiliente .
                        Essa ferramenta lhe permitirá visualizar melhor o seu negócio agindo nas áreas mais críticas atualmente.
                        </p>
                    </div>
                </div>
            </div>

            <div class="destaque">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="texto">
                                <h1 class="destaque-baixo">Clique no botão abaixo e receba a Ferramenta  6 ”Rs” Gratuitamente!</h1>
                                
                                <div class="botao">
                                    <button class="minha-ferramenta">Quero Minha Ferramenta Agora!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="pg pg-popup">
            <a href="https://www.google.com" class="linkPdf"></a>
            <div class="container">
                <div class="newsletter">
                    <div class="fechar">X</div>
                    <h2>Para receber sua ferramenta gratuita diretamente em seu email, entre com seus dados abaixo:</h2>

                    <!--START Scripts : this is the script part you can add to the header of your theme-->
                    <script type="text/javascript" src="http://sejagigante.com.br/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
                    <script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
                    <script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
                    <script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
                    <script type="text/javascript">
                        /* <![CDATA[ */
                        var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://sejagigante.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
                        /* ]]> */
                    </script>
                    <script type="text/javascript" src="http://sejagigante.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
                    <!--END Scripts-->

                    <div class="widget_wysija_cont html_wysija">
                        <div id="msg-form-wysija-html5bfc3779391c9-5" class="wysija-msg ajax"></div>


                        <form id="form-wysija-html5bfc3779391c9-5" method="post" action="#wysija" class="widget_wysija html_wysija">

                            <input type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="Nome" placeholder="Nome" value="" />

                            <span class="abs-req">
                                <input type="text" name="wysija[user][abs][firstname]" class="wysija-input hidden validated[abs][firstname]" value="" />
                            </span>

                            <input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />

                            <span class="abs-req">
                                <input type="text" name="wysija[user][abs][email]" class="wysija-input hidden validated[abs][email]" value="" />
                            </span>

                            <input type="text" name="wysija[field][cf_1]" class="wysija-input validate[required,custom[phone]] phoneMask" title="Telefone" placeholder="Telefone" value="" />
                            
                            <span class="abs-req">
                                <input type="text" name="wysija[field][abs][cf_1]" class="wysija-input hidden validated[abs][cf_1]" value="" />
                            </span>

                            <input class="wysija-submit wysija-submit-field botao" type="submit" value="Quero Minha Ferramenta Agora!" />
                            <input type="hidden" name="form_id" value="5" />
                            <input type="hidden" name="action" value="save" />
                            <input type="hidden" name="controller" value="subscribers" />
                            <input type="hidden" value="1" name="wysija-page" />
                            <input type="hidden" name="wysija[user_list][list_ids]" value="3" />
                        </form>
                        <span>Respeitamos a privacidade dos seus dados, e não repassaremos a terceiros.</span>
                    </div>
                </div>
            </div>
        </div>

   

    <script>
       $('button.minha-ferramenta').click(function(){
        $('.pg-popup').slideDown();
    });

       $('.fechar').click(function(){
        $('.pg-popup').slideUp();
    });

       $('.wysija-submit.wysija-submit-field.botao').click(function(){

            setTimeout(function(){ 
               
                if($("div.formErrorContent").length >= 1 || $("div.error").length >= 1) {
                   console.log("error");

                }else{
                      window.location.href="http://sejagigante.com.br/wp-content/uploads/2018/11/Ferramenta-6-Rs.pdf";
                      console.log("Deu super certo");
                }

            }, 4000);
        });

    </script>
    <script type='text/javascript' src='//code.jquery.com/jquery-compat-git.js'></script>
    <script type='text/javascript' src='//igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script>
    <script>
        var behavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(behavior.apply({}, arguments), options);
            }
        };

        $('.phoneMask').mask(behavior, options);
    </script>



<?php get_footer(); ?>