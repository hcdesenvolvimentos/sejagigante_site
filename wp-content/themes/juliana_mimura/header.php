<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Juliana_Mimura
 */

global $configuracao;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=K2D:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="https://matthewelsom.com/assets/js/libs/instafeed.min.js"></script>
	<?php wp_head(); ?>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129588314-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129588314-1');
</script>

</head>

<body <?php body_class(); ?>>
<div class="whats">
			<a href="https://api.whatsapp.com/send?l=pt&phone=<?php echo $configuracao['opt_wpp'] ?>" target="_blank">
				<i class="fab fa-whatsapp"></i>
				Informações por WhatsApp
			</a>
		</div>
		<header class="topo">
			<div class="container">
				<a href="<?php echo get_home_url() ?>">
					<img src=" <?php echo $configuracao['opt_logo']['url'] ?> " alt="">
					<!-- <p>Estratégia para pequenos negócios.</p> -->
				</a>
				<div class="buttonMobile">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</div>
				<nav>
					<div class="menuList">

						<?php 
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu principal',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
							);
							wp_nav_menu( $menu );
						?>

						<!-- <ul>
							<li>
								<a class="scrollTop" href="#sobre">Home</a>
							</li>
							<li style="display:none" class="dow homemenu">
								<a class="scrollTop" href="<?php // echo get_home_url() ?>">Home</a>
							</li>
							<li class="list-submenu">
								<a class="scrollTop" href="#programas">Programas</a>
								<ul class="submenu">
									<li>
										<a class="scrollTop" href="#euGigante">Eu Gigante</a>
									</li>
									<li>
										<a class="scrollTop" href="#empresaGigante">Empresa Gigante</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="scrollTop" href="#resultados">Resultados</a>
							</li>
							
							<li>
								<a class="scrollTop" href="#contato">Contato</a>
							</li>
							<li class="dow">
								<a style="background: #921521;
    color: #fff;" href="http://sejagigante.com.br/downloads/">Downloads</a>
							</li>
						</ul> -->

					</div>
				</nav>
			</div>
		</header>